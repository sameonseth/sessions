import React, { useState } from 'react';
import { Form, Button } from 'react-bootstrap';

const SearchCourseByPrice = () => {
  const [minPrice, setMinPrice] = useState('');
  const [maxPrice, setMaxPrice] = useState('');
  const [searchResults, setSearchResults] = useState([]);

  const handleSearch = async () => {
    // Make a Fetch API request to your backend
    try {
      const response = await fetch(`${process.env.REACT_APP_API_URL}/courses/searchByPrice`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({ minPrice, maxPrice })
      });
      const data = await response.json();
      setSearchResults(data.courses);
      console.log(data);
    } catch (error) {
      console.error('Error searching for courses:', error);
    }
  };

  return (
    <div className="my-5">
      <h2>Course Search By Price Range</h2>
      <Form>
        <Form.Group controlId="minPrice">
          <Form.Label>Minimum Price</Form.Label>
          <Form.Control
            type="number"
            placeholder="Enter minimum price"
            value={minPrice}
            onChange={e => setMinPrice(e.target.value)}
          />
        </Form.Group>
        <Form.Group controlId="maxPrice">
          <Form.Label>Maximum Price</Form.Label>
          <Form.Control
            type="number"
            placeholder="Enter maximum price"
            value={maxPrice}
            onChange={e => setMaxPrice(e.target.value)}
          />
        </Form.Group>
        <Button variant="primary" onClick={handleSearch}>
          Search
        </Button>
      </Form>
      <h3>Search Results:</h3>
      <ul>
        {searchResults.map(course => (
          course.isActive ?
            <li key={course._id}>{course.name}</li>
          :
            null
        ))}
      </ul>
    </div>
  );
};

export default SearchCourseByPrice;
