import PropTypes from 'prop-types';
//import {useState} from 'react';
import {Card} from 'react-bootstrap';
import {Link} from 'react-router-dom';

//destructured course property from props
export default function CourseCard({course}){
	//destructured name, description, and price from the course property of props
	const {name, description, price, _id} = course;

	//count - getter, setCount - setter
	//line below is equivalent to: let count = 0;
	//instead of doing: count=5, to change the value; use setCount(5) instead
	//useState() hook is responsible for setting the initial value of the state
	
	// const [count, setCount] = useState(0);
	// const [seats, setSeats] = useState(30);

	// function enroll(){
	// 	if(seats === 0){
	// 		return alert("No more seats available.");
	// 	}
	// 	setCount(prev_value => prev_value + 1);
	// 	setSeats(prev_value => prev_value - 1);
	// }

	return (
		<Card className="cardHighlight" id="courseComponent1">
		    <Card.Body>
		        <Card.Title>{name}</Card.Title>
		        <Card.Subtitle>Description:</Card.Subtitle>
		        <Card.Text>{description}</Card.Text>
		        <Card.Subtitle>Price:</Card.Subtitle>
		        <Card.Text>PhP {price}</Card.Text>
		        <Link className="btn btn-primary" to={`/courses/${_id}`}>Details</Link>
		    </Card.Body>
		</Card>
	)
}

//PropTypes is used for validating the data from the props
CourseCard.propTypes = {
	course: PropTypes.shape({
		name: PropTypes.string.isRequired,
		description: PropTypes.string.isRequired,
		price: PropTypes.number.isRequired
	})
}