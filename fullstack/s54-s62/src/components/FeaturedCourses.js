import {useState, useEffect} from 'react';
import {CardGroup} from 'react-bootstrap';
import PreviewCourses from './PreviewCourses.js';

export default function FeaturedCourses() {

	const [previews, setPreviews] = useState([]);

	const [noCourses, setNoCourses] = useState(false);

	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/courses/`)
		.then(response => response.json())
		.then(data => {
			console.log(data);

			if(data.length === 0) setNoCourses(true);

			//These variables will be used to store the random numbers and featured courses data.
			const numbers = [];
			const featured = [];

			const generateRandomNums = () => {
				let randomNum = Math.floor(Math.random() * data.length);
				if(numbers.indexOf(randomNum) === -1){
					numbers.push(randomNum)
				}
				else {
					generateRandomNums();
				}
			}

			for(let i=0; i < (data.length >= 5 ? 5 : data.length); i++){
				generateRandomNums();

				featured.push(
					<PreviewCourses data={data[numbers[i]]} key={data[numbers[i]._id]} breakPoint={2}/>
				)
			}

			setPreviews(featured);
		})
	}, []);

	return (
		noCourses ?
			null
		:
			<>
				<h2 className="text-center">Featured Courses</h2>
				<CardGroup className="justify-content-center">
					{previews}
				</CardGroup>
			</>
	)
}