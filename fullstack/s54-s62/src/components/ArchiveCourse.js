import {Button} from 'react-bootstrap';
import Swal from 'sweetalert2';

export default function ArchiveCourse({course, fetchData}){
    const archiveToggle = (courseId) => {
        console.log(courseId);
        fetch(`${process.env.REACT_APP_API_URL}/courses/${courseId}/archive`, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then(response => response.json())
        .then(data => {
            console.log(data);
            if(data === true){
                Swal.fire({
                    title: "Success!",
                    icon: "success",
                    text: "Course archived successfully"
                });
                fetchData();
            }
            else {
                Swal.fire({
                    title: "Error!",
                    icon: "error",
                    text: "Please try again."
                });
            }
        });
    }

    const activateToggle = (courseId) => {
        console.log(courseId);
        fetch(`${process.env.REACT_APP_API_URL}/courses/${courseId}/activate`, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then(response => response.json())
        .then(data => {
            console.log(data);
            if(data === true){
                Swal.fire({
                    title: "Success!",
                    icon: "success",
                    text: "Course activated successfully"
                });
                fetchData();
            }
            else {
                Swal.fire({
                    title: "Error!",
                    icon: "error",
                    text: "Please try again."
                });
            }
        });
    }

	return (
        course.isActive ?
            <Button variant="danger" size="sm" onClick={() => archiveToggle(course.id)}>Archive</Button>
    	:	
            <Button variant="success" size="sm" onClick={() => activateToggle(course.id)}>Activate</Button>
	)
}