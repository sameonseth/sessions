import {useState, useEffect} from 'react';
import {Table} from 'react-bootstrap';
import EditCourse from './EditCourse.js';
import ArchiveCourse from './ArchiveCourse.js';

export default function AdminView({coursesData, fetchData}){
	const [courses, setCourses] = useState([]);

	useEffect(() => {
		setCourses(coursesData.map(course => {
			return (
				<tr key={course._id}>
					<td>{course._id}</td>
		            <td>{course.name}</td>
		            <td>{course.description}</td>
		            <td>{course.price}</td>
		            {
						course.isActive !== false ?
							<td className="text-success">Available</td>
						:
							<td className="text-danger">Unavailable</td>
		            }
		            <td><EditCourse course={course._id} fetchData={fetchData}/></td>
		           	<td><ArchiveCourse course={{id: course._id, isActive: course.isActive}} fetchData={fetchData}/></td>
	            </tr>
			)
		}))
	}, [coursesData, fetchData])

	return (
		<>
			<h1 className="text-center my-5">Admin Dashboard</h1>
			<Table striped bordered hover responsive>
				<thead className="text-center">
			        <tr>
			          	<th>ID</th>
			          	<th>Name</th>
			          	<th>Description</th>
			          	<th>Price</th>
			          	<th>Availability</th>
			          	<th colSpan={2}>Actions</th>
			        </tr>
				</thead>
				<tbody>
					{courses}
				</tbody>
		    </Table>
		</>
	)
}