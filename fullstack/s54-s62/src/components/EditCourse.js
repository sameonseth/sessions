import {useState} from 'react';
import {Button, Modal, Form} from 'react-bootstrap';
import Swal from 'sweetalert2'

export default function EditCourse({course, fetchData}){
	//State hooks

	//State of the courseId for the fetch URL
	const [courseId, setCourseId] = useState("");

	//Form states
	const [name, setName] = useState("");
    const [description, setDescription] = useState("");
    const [price, setPrice] = useState("");

    //State for editCourse Modals to open/close
    const [showEdit, setShowEdit] = useState(false);

    const openEdit = (courseId) => {
    	fetch(`${process.env.REACT_APP_API_URL}/courses/${courseId}`)
        .then(response => response.json())
        .then(data => {
        	setShowEdit(true);
        	setCourseId(data._id);
        	setName(data.name);
            setDescription(data.description);
            setPrice(data.price);
        });
    }

    const closeEdit = () => {
    	setShowEdit(false);
    	setName("");
        setDescription("");
        setPrice(0);
    }

    const editCourse = (event, courseId) => {
    	event.preventDefault();
    	fetch(`${process.env.REACT_APP_API_URL}/courses/${courseId}`, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify({
                name: name,
                description: description,
                price: price
            })
        })
        .then(response => response.json())
        .then(data => {
            console.log(data);
            if(data === true){
                Swal.fire({
                    title: "Success!",
                    icon: "success",
                    text: "Course updated successfully"
                });
            	closeEdit();
                fetchData();
            }
            else {
                Swal.fire({
                    title: "Error!",
                    icon: "error",
                    text: "Please try again."
                });
            	closeEdit();
            }
        });
    }

	return (
		<>
			<Button variant="primary" size="sm" onClick={() => openEdit(course)}>Edit</Button>
			<Modal show={showEdit} onHide={closeEdit}>
				<Form onSubmit={(event) => editCourse(event, courseId)}>
                    <Modal.Header>
                    	<Modal.Title>Edit Course</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                    	<Form.Group controlId="courseName">
	                        <Form.Label>Name</Form.Label>
	                        <Form.Control type="text" placeholder="Enter Name" value={name} onChange={event => setName(event.target.value)} required/>
	                    </Form.Group>
	                    <Form.Group controlId="courseDescription">
	                        <Form.Label>Description</Form.Label>
	                        <Form.Control type="text" placeholder="Enter Description" value={description} onChange={event => setDescription(event.target.value)} required/>
	                    </Form.Group>
	                    <Form.Group controlId="coursePrice">
	                        <Form.Label>Price</Form.Label>
	                        <Form.Control type="number" placeholder="Enter Price" value={price} onChange={event => setPrice(event.target.value)} required/>
	                    </Form.Group>
                    </Modal.Body>  
                    <Modal.Footer>
                    	<Button variant="secondary" onClick={closeEdit}>Close</Button>
                    	<Button variant="success" type="submit">Submit</Button>
                    </Modal.Footer>           
                </Form>
			</Modal>
		</>
	)
}