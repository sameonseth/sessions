import React, { useState, useEffect } from 'react';
import { Button, Form } from 'react-bootstrap';
import Swal from 'sweetalert2';

function ProfileUpdate({fetchData}) {
  const [firstName, setFirstName] = useState('');
  const [lastName, setLastName] = useState('');
  const [mobileNo, setMobileNo] = useState('');
  const [isActive, setIsActive] = useState(false);

  useEffect(() => {
  	setIsActive((firstName && lastName && mobileNo) && mobileNo.length === 11);
  }, [firstName, lastName, mobileNo]);

  const handleSubmit = () => {
    const updatedProfile = {
      firstName: firstName,
      lastName: lastName,
      mobileNo: mobileNo
    };

    fetch(`${process.env.REACT_APP_API_URL}/users/profile`, {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`
      },
      body: JSON.stringify(updatedProfile),
    })
      .then(response => response.json())
      .then(data => {
        if (data) {
          Swal.fire('Success', 'Profile updated successfully!', 'success');
          fetchData();
        } else {
          Swal.fire('Error', 'Failed to update profile.', 'error');
        }
        setFirstName("");
        setLastName("");
        setMobileNo("");
      })
      .catch(error => {
        console.error('Error updating profile:', error);
        Swal.fire('Error', 'An error occurred while updating your profile.', 'error');
      });
  };

  return (
  	<>
  		<h2>Update Profile</h2>
  		<Form>
	      <Form.Group controlId="firstName">
	        <Form.Label>First Name</Form.Label>
	        <Form.Control
	          type="text"
	          placeholder="Enter your first name"
	          value={firstName}
	          onChange={e => setFirstName(e.target.value)}
	        />
	      </Form.Group>

	      <Form.Group controlId="lastName">
	        <Form.Label>Last Name</Form.Label>
	        <Form.Control
	          type="text"
	          placeholder="Enter your last name"
	          value={lastName}
	          onChange={e => setLastName(e.target.value)}
	        />
	      </Form.Group>

	      <Form.Group controlId="mobileNo">
	        <Form.Label>Mobile Number</Form.Label>
	        <Form.Control
	          type="tel"
	          placeholder="Enter your mobile number"
	          value={mobileNo}
	          onChange={e => setMobileNo(e.target.value)}
	        />
	      </Form.Group>

	      <Button variant="primary" onClick={handleSubmit} disabled={!isActive}>
	        Update Profile
	      </Button>
	    </Form>
  	</>
	    
  );
}

export default ProfileUpdate;