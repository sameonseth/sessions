import {useState, useEffect} from 'react';
import CourseCard from './CourseCard.js';
import CourseSearch from './CourseSearch.js';
import CourseSearchByPrice from './CourseSearchByPrice.js';

export default function UserView({coursesData}){
	const [courses, setCourses] = useState([]);

	useEffect(() => {
		setCourses(coursesData.map(course_item => {
			return (
				(course_item.isActive !== false) ?
					<CourseCard key={course_item._id} course={course_item}/>
				:
					null	
			)
		}))
	}, [coursesData])

	return (
		<>
			<h1 className="text-center">Courses</h1>
			<CourseSearch/>
			<CourseSearchByPrice/>
			{courses}
		</>
	)
}