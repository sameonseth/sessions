import Banner from '../components/Banner.js';
import FeaturedCourses from '../components/FeaturedCourses.js';
import Highlights from '../components/Highlights.js';

export default function Home(){
	const data = {
		title: "Earl's Fishing Academy",
		content: "Fish for everyone!",
		destination: "/courses",
		label: "Enroll now!"
	}
	return (
		<>
			<Banner data={data}/>
			<FeaturedCourses/>
			<Highlights/>
		</>
	)
}