import {useContext, useState, useEffect} from 'react';
import {Row, Col} from 'react-bootstrap';
import {Navigate} from 'react-router-dom';
import ResetPassword from '../components/ResetPassword.js';
import UpdateProfile from '../components/UpdateProfile.js';
import UserContext from '../UserContext';


export default function Profile(){

    const {user} = useContext(UserContext);

    const [details, setDetails] = useState({});

    const fetchData = () => {
        fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
            headers: {
                Authorization: `Bearer ${ localStorage.getItem('token') }`
            }
        })
        .then(response => response.json())
        .then(data => {
            console.log(data)
            // Set the user states values with the user details upon successful login.
            if (typeof data._id !== "undefined") setDetails(data);
        });

    }

    useEffect(() => {
		fetchData();
	}, []);

    return (
		(user.id !== null) ?
			<>
				<Row className="text-white">
					<Col className="bg-primary p-5">
						<h1 className="my-5">Profile</h1>
						<h2>{`${details.firstName} ${details.lastName}`}</h2>
						<hr />
						<h3>Contacts</h3>
						<ul>
							<li>Email: {details.email}</li>
							<li>Mobile No: {details.mobileNo}</li>
						</ul>
					</Col>
				</Row>
				<Row className="pt-4 mt-4">
	                <Col>
	                    <UpdateProfile fetchData={fetchData}/>
	                </Col>
	            </Row>
				<Row className="pt-4 mt-4">
	                <Col>
	                    <ResetPassword />
	                </Col>
	            </Row>
			</>
		:
			<Navigate to="/" /> 
	)
}