import {useState, useEffect, useContext} from 'react';
import {Form, Button} from 'react-bootstrap';
import {Navigate, useNavigate} from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext.js';

export default function AddCourse(){
    const {user} = useContext(UserContext);
    const navigate = useNavigate();
    //States
	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState("");
	const [isActive, setIsActive] = useState(false);

    useEffect(() => {
        setIsActive(name && description && price);
    }, [name, description, price]);

    function addCourse(event){
        //Prevents page load upon form submission
        event.preventDefault();
        fetch(`${process.env.REACT_APP_API_URL}/courses/`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify({
                name: name,
                description: description,
                price: price
            })
        })
        .then(response => response.json())
        .then(data => {
            console.log(data);
            if(data === true){
                Swal.fire({
                    title: "Course Added",
                    icon: "success"
                });
                navigate("/courses");
            }
            else {
                Swal.fire({
                    title: "Unsuccessful Course Creation",
                    icon: "error",
                    text: data.message
                });
            }
        });
        setName("");
        setDescription("");
        setPrice("");
    };

	return (
        (user.id === null || user.isAdmin === false) ?
            <Navigate to="/courses" />
        :  
            <>
        		<Form onSubmit={(event) => addCourse(event)}>
                    <h1 className="my-5 text-center">Add Course</h1>
                    <Form.Group>
                        <Form.Label>Name:</Form.Label>
                        <Form.Control type="text" placeholder="Enter Name" name="name" value={name} onChange={event => setName(event.target.value)} required/>
                    </Form.Group>
                    <Form.Group>
                        <Form.Label>Description:</Form.Label>
                        <Form.Control type="text" placeholder="Enter Description" name="description" value={description} onChange={event => setDescription(event.target.value)} required/>
                    </Form.Group>
                    <Form.Group>
                        <Form.Label>Price:</Form.Label>
                        <Form.Control type="number" placeholder="Enter Price" name="price" value={price} onChange={event => setPrice(event.target.value)} required/>
                    </Form.Group>
                    <Button className="mt-5" variant="primary" type="submit" disabled={!isActive}>Submit</Button>
                        
                </Form>
            </>       
	)
}