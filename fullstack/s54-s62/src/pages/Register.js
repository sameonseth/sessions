import {useState, useEffect, useContext} from 'react';
import {Form, Button} from 'react-bootstrap';
import {Navigate} from 'react-router-dom';
import UserContext from '../UserContext.js';

export default function Register(){
    const {user} = useContext(UserContext);
	const [firstName, setFirstName] = useState("");
	const [lastName, setLastName] = useState("");
	const [email, setEmail] = useState("");
	const [mobileNo, setMobileNo] = useState("");
	const [password, setPassword] = useState("");
	const [confirmPassword, setConfirmPassword] = useState("");
	const [isActive, setIsActive] = useState(false);

    useEffect(() => {
        //Insert effect here
        setIsActive((firstName && lastName && email && mobileNo && password && confirmPassword) && (password === confirmPassword) && (mobileNo.length === 11));
    }, [firstName, lastName, email, mobileNo, password, confirmPassword]);

    function registerUser(event){
        //Prevents page load upon form submission
        event.preventDefault();
        fetch('http://localhost:4000/users/register', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                firstName: firstName,
                lastName: lastName,
                email: email,
                mobileNo: mobileNo,
                password: password
            })
        }).then(response => response.json()).then(result => {
            if(result){
                setFirstName("")
                setLastName("")
                setEmail("")
                setMobileNo("")
                setPassword("")
                setConfirmPassword("")
                if(result){
                    alert(`User ${firstName} ${lastName} registered successfully!`)
                }
                else{
                    alert("User registration failed.")
                }
            }
            else {
                alert("Please try again :(")
            }
        });
    };

	return (
        (user.id !== null) ?
            <Navigate to="/courses" />       
        :
            <>
        		<Form onSubmit={(event) => registerUser(event)}>
                    <h1 className="my-5 text-center">Register</h1>
                    <Form.Group>
                        <Form.Label>First Name:</Form.Label>
                        <Form.Control type="text" placeholder="Enter First Name" name="firstName" value={firstName} onChange={event => setFirstName(event.target.value)} required/>
                    </Form.Group>
                    <Form.Group>
                        <Form.Label>Last Name:</Form.Label>
                        <Form.Control type="text" placeholder="Enter Last Name" name="lastName" value={lastName} onChange={event => setLastName(event.target.value)} required/>
                    </Form.Group>
                    <Form.Group>
                        <Form.Label>Email:</Form.Label>
                        <Form.Control type="email" placeholder="Enter Email" name="email" value={email} onChange={event => setEmail(event.target.value)} required/>
                    </Form.Group>
                    <Form.Group>
                        <Form.Label>Mobile No:</Form.Label>
                        <Form.Control type="number" placeholder="Enter 11 Digit No." name="mobileNo" value={mobileNo} onChange={event => setMobileNo(event.target.value)} required/>
                    </Form.Group>
                    <Form.Group>
                        <Form.Label>Password:</Form.Label>
                        <Form.Control type="password" placeholder="Enter Password" name="password" value={password} onChange={event => setPassword(event.target.value)} required/>
                    </Form.Group>
                    <Form.Group>
                        <Form.Label>Confirm Password:</Form.Label>
                        <Form.Control type="password" placeholder="Confirm Password" name="confirmPassword" value={confirmPassword} onChange={event => setConfirmPassword(event.target.value)} required/>
                    </Form.Group>
                    <Button variant="primary" type="submit" disabled={!isActive}>Submit</Button>
                        
                </Form>
            </>
	)
}