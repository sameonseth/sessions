import {useEffect, useState, useContext} from 'react';
import AdminView from '../components/AdminView.js';
import UserView from '../components/UserView.js';
import UserContext from '../UserContext.js';

export default function Courses(){
	const {user} = useContext(UserContext);
	//State that will be used to store the courses retrieved from the databases.
	const [courses, setCourses] = useState([]);

	const fetchData = () => {
		fetch(`${process.env.REACT_APP_API_URL}/courses/all`)
		.then(response => response.json())
		.then(data => {
			console.log(data);
			setCourses(data);
		});
	}

	//Retrieves the courses from the DB upon initial render of the "Courses" component
	useEffect(() => {
		fetchData();
	}, []);

	return (
		(user.isAdmin === true) ?
			<AdminView coursesData={courses} fetchData={fetchData}/>
		:
			<UserView coursesData={courses}/>
	)
}

