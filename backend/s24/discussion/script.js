//[SECTION] While loop
let count = 5;
while(count != 0){
	console.log("Current value of count: " + count);
	count--;
}

//[SECTION] Do-while loop
let number = Number(prompt("Give me a number:"));
do{
	console.log("Current value of number: " + number);
	number += 1;
}while(number < 10);

//[SECTION] For loop
for(let count = 0; count <= 20; count++){
	console.log("Current for loop value: " + count);
}

let my_string = "earl";
//To get the length of a string
console.log(my_string.length);
//To get a specific letter in a string
console.log(my_string[0]);
//Loops through each letter in the string and will keep iterating as long as the current index is less than the length of the string.
for(let index = 0; index < my_string.length; index++){
	console.log(my_string[index]); //starts at letter 'e' and will keep printing each letter up to 'l'.
}

//MINI ACTIVITY (20 mins)
//1. Loop through the 'my_name' variable which has a string with your name on it.
//2. Display each letter in the console but EXCLUDE all the vowel from it.
//3. Send a screenshot of the output in our B303 google hangouts
let my_name = "Seth Sameon";
for(let index = 0; index < my_name.length; index++){
	if(my_name[index] != 'a' && my_name[index] != 'e' && my_name[index] != 'o'){
		console.log(my_name[index]);
	}
}

let name_two = "rafael";

for(let index = 0; index < name_two.length; index++){
	//If the current letter is 'a' then it will reiterate the loop
	if(name_two[index].toLowerCase() == 'a'){
		console.log("Skipping...");
		continue;
	}
	//If the current letter is 'e' then it will break/stop the whole loop entirely
	if(name_two[index] == 'e'){
		break;
	}
	console.log(name_two[index]);
}