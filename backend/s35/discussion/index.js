//Greater than operator
//gt - greater than, gte - greater than & equal
db.users.find(
    {
        age: {
            $gte: 82
        } 
    }
);

//Lesser than operator
//lt - lesser than, lte - lesser than & equal
db.users.find(
    {
        age: {
            $lte: 82
        } 
    }
);

//Regex operator
//regex - pattern, options 'i' - case insensitive
db.users.find({
    firstName: {
        $regex: 's',
        $options: 'i'
    }
});

//Regex operator
db.users.find({
    lastName: {
        $regex: 'T',
        $options: 'i'
    }
});

//Combining Operators
db.users.find({
    age: {
        $gt: 70
    },
    lastName: {
        $regex: 'g'
    }
});

db.users.find({
    age: {
        $lte: 76
    },
    firstName: {
        $regex: 'j',
        $options: 'i'
    }
});

//Field Projection
db.users.find({}, {
    "_id": 0
});

db.users.find({}, {
    "_id": 0,
    "firstName": 1
});