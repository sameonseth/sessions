console.log("Mutator Methods");

//Iteration Methods
/*
	- loops through all the tasks
*/

//forEach() - to loop through the array
//map() - loops through the array and returns a new array
//filter() - returns a new array containing elements which meets the given condition

//every() - checks if all elements meet the given condition
//returns true if all elements meet the given condition, however, false if it does not
let numbers = [1, 2, 3, 4, 5, 6];
let allValid = numbers.every(function(number){
	return number > 3;
});
console.log("Result of every() method: ");
console.log(allValid);

//some() - checks if at least one element meets the given condition
//returns true if at least one element meets the given condition, return false otherwise
let someValid = numbers.some(function(number){
	return number < 2;
});
console.log("Result of some() method: ");
console.log(someValid);

//includes() - methods can be "chained" using them one after another
let products = ['Mouse', 'Keyboard', 'Laptop', 'Monitor'];
let filteredProducts = products.filter(function(product){
	return product.toLowerCase().includes('a');
})
console.log(filteredProducts);

//reduce()
let iteration = 0;
let reducedArray = numbers.reduce(function(x,y){
	console.warn("Current iteration: " + ++iteration);
	console.log("Accumulator: " + x);
	console.log("currentValue: " + y);
	return x + y;
})
console.log("Result of reduce method: " + reducedArray);
let productsReduce = products.reduce(function(x,y){
	return x + '' + y;
});
console.log("Result of reduce() method: " + productsReduce);